const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            // Variaveis para armazenamento das entradas do formulário da página de cadastro
            newUsername:"",
            newPassword:"",
            confirmPassword:"",

            //Declaração dos arrays para armazenamento de usuários e senhas
            usuarios: ["admin", "guilherme", "gustavo"],
            senhas:["123", "123", "123"],
            mostrarEntrada: false,
            mostrarLista: false,
        }
    },//Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                //alert("Dentro do SetTimeout!")
                if((this.username === "Guilherme" && this.password === "123456789")||
                (this.username === "Gustavo" & this.password === "123") ||
                (this.username === "Admin" & this.password === "321")){
                    this.sucesso = "login efetuado com sucesso!";
                    this.error = null;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    // alert("Login não efetuado");
                    this.error = "Nome ou senha incorretos";
                    this.sucesso = null;
                }
            }, 1000);
            //alert("Saiu do setTimeout!!!");
        },//Fechamenro Login

        login2(){
            this.mostrarEntrada = false;

            if (localStorage.getItem("usuarios")&& localStorage.getItem("senhas")) {
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }


            setTimeout(() => {
                this.mostrarEntrada = true
                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);
                    this.error = null;
                    this.sucesso = "login efetuado com sucesso!";
                    if(this.username === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    }
                }//Fechamento do if
                else{
                    this.error = "Nome ou senha incorretos";
                    this.sucesso = null
                }
                
                this.username = "";
                this.password = "";

            }, 500);            

        },//Fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.sucesso="login ADM identificado!";
                this.error= null;
                this.mostrarEntrada = true;

                // localStorage.setItem("username", this.username);
                // localStorage.setItem("password", this.password);

                setTimeout(() => {}, 2000);

                setTimeout (() => {
                    window.location.href = "paginaCadastro.html";
                },1000); // Fechamento setTimeout
            }
            else{
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "Faça login com usuário administrativo!";
                }, 500);//Fechamento setTimeout
            }//Fechamento else
        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");
            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.username === "admin"){
                    if(this.newUsername !== ""){
                        if (!this.usuarios.includes(this.newUsername)){
                            if(this.newPassword && this.newPassword === this.confirmPassword){
                                this.usuarios.push(this.newUsername);
                                this.senhas.push(this.newPassword);

                                //Armazenamento do usuário e senha no LocalStorage
                                localStorage.setItem("usuarios",JSON.stringify(this.usuarios));

                                localStorage.setItem("senhas",JSON.stringify(this.senhas));

                                this.error = null;
                                this.sucesso = "Usuário cadastrado com sucesso";
                            }//Fechamento do if
                            else{
                                this.sucesso = null;
                                this.error = "Por favor, confirme sua senha!!!";
                            }//Fechamento do else
                        }//Fechamento if includes
                        else{
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado";
                        }//Fechamento do else
                    }//Fechamento if diferente de branco
                    else{
                        this.error = "Por favor digte o nome do usário!";
                        this.sucesso = null;
                        
                    }//Fechamento else
                    
                }
                else{
                    this.error = "Usuário não ADM!!!";
                    this.sucesso = null;
                }//Fechamento else

                this.newUsername = "";
                this.newPassword = "";
                this.confirmPassword = "";

            }, 500);//Fechamento setTimeout
        }, //Fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));

            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Vechamento verCadastrados

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario == "admin"){
                //Impedir a exclusão do usuario admin
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "O usuário ADMIN não pode ser exluido!!!";
                }, 500);
                return;//Força para a finalização do bloco/função

            }//Fechamento if usuario
            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);

                    //Atualização dos vetores no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }//Fechamento index

            }//Fechmamento do if confirm
        },//Fechamento exclusirUsuario
    },//Fechamento metods

}).mount("#app");