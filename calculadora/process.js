const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoDisplay: 10,
            tamanhoLetra: 50 + "px",
        };//Fechamento return
    },//Fechameto data

    methods:{
        numero(valor){
            this.displayResponsivo();

            if(this.display == "0"){
                this.display = valor.toString();
            }

            
            else{

                if(this.operador == "="){
                    this.display = '';
                    this.operador = null;
                }
                //this.display = this.display + valor.toString();
                this.display += valor.toString();//Formula resumida da acima
                // if(parseFloat(this.display + valor).toString().length <= 10) {
                //     this.display += valor.toString();
                // }FOI ESSE VALOR QUE O CHATGPT RECOMENDOU, SUBSTITUINDO TAMBÉM A LINHA 25, MAS NÃO FUNCIONOU
            }

        },//Fechamento numero
        decimal(){
            if(!this.display.includes(".")){
            //this.display = this.display + ".";
            this.display += ".";//Formula resumida da acima
            }
        },//Fechamento decimal

        clear(){
            this.display = "0";
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.operador = null;
            this.displayResponsivo();
        },//Fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.numeroAtual * displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.numeroAtual / displayAtual).toString();
                        break;
                        // case "/":
                        //     const resultado = this.numeroAtual / displayAtual;
                        //     if (isNaN(resultado)) {
                        //         this.display = "Indefinido";
                        //     } else {
                        //         this.display = resultado.toString();
                        //     }
                        //     break; FOI OQUE O CHAT GPT RECOMENDOU PARA SUBSTIUIR A MENSAGEM AO DIVIDIR 0 POR 0, SUBSTITUINDO TODO O CASE "/" POR CIMA DO 59
                }// Fim do switch
                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;

                if(this.display == "NaN"){
                    this.display = "Não existe";
                }
                else if (this.display == "Infinity"){
                    this.display = "Infinito!"
                }

            }// Fim do if

            if(operacao != "="){

                this.operador = operacao;
                this.numeroAtual = parseFloat(this.display);
                //parseFloat = número quebrado(e inteiro), parseInt = número inteiro
                this.display = "0";
            }
            else{
                this.operador = operacao;
                if(this.display.includes(".")){
                    const displayAtual = parseFloat
                    (this.display);
                    this.display = (displayAtual.toFixed(2)).toString();
                }
            }
            this.displayResponsivo();

            //alert("O numero atual é: " + this.numeroAtual);
            // alert("O operador é: " + this.operador);
        },//Fechamento Operacoes

        displayResponsivo(){
            if(this.display.length >= 28){
                this.tamanhoLetra = 14 +"px";
            }
            else if(this.display.length > 18){
                this.tamanhoLetra = 20 +"px";
            }
            else if(this.display.length > 10){
                this.tamanhoLetra = 30 +"px"
            }
            else[
                this.tamanhoLetra = 50 + "px"
            ]

        },//Fechamento displayResponsivo
        
    },//Fechamento de methods
    
}).mount("#app");//Fechamento createApp