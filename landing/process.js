const {createApp} = Vue;

createApp({
    data(){
        return{
            products:[
                {
                    id:1,
                    name: "Tênis",
                    description: "Um par de tênis confrotável para esportes",
                    price: 129.99,
                    image: "./imagens/imgTenis.jpg",
                },// Fechamento item 1
                {
                    id:2,
                    name: "Botas",
                    description: "Botas elegantes para qualquer ocasião",
                    price: 199.99,
                    image: "./imagens/imgBotas.jpg",
                },// Fechamento item 2
                {
                    id:3,
                    name: "Sapatos",
                    description: "Sapatos clássicos para visual sofisticado!",
                    price: 149.99,
                    image: "./imagens/imgSapatos.jpg",
                },// Fechamento item 3
                {
                    id: 4,
                    name: "Sandália",
                    description: "Sandálias confortáveis para os seus pés!",
                    price: 69.99,
                    image: "./imagens/imgSandalias.jpg",
                },//Fechamento item 4
            ],// Fechamento products
            currentProduct: {}, //Produto atual
            cart: [],

        };// Fechamento return
    },// Fechamento data

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();

    },//Fechamento mounted

    //Função VUE para retornar resultados específicos de um bloco programado
    computed:{
        cartItemCount(){
            return this.cart.length;
        },//Fechamento cartItemCount

        cartTotal(){
            return this.cart.reduce((total, product)=> total + product.price, 0);
        },//Fechamento cartTotal
    },//Fechamento computed

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};

        },//Fechamento updateProduct

        addToCart(product){
            this.cart.push(product);
        },//Fechamento addToCart

        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
                this.cart.splice(index,1);
            }
        },//Fechamento remove
    },//Fechamento metods
}).mount("#app");//Fechamento createApp
